import { Server, Socket } from "socket.io";
import logger from './utils/logger';

function socket({ io }: {io: Server}){
    logger.info("Sockets enabled");
}

export default socket;