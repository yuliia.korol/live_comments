import { createContext, FC, useContext } from 'react';
import io, { Socket } from 'socket.io-client';

const SOCKET_URL = 'http://localhost:4000';
const socket = io(SOCKET_URL);

const SocketContext = createContext<Socket>(socket);

const SocketProvider: FC = ({ children }) => {
  return (
    <SocketContext.Provider value={socket}>{children}</SocketContext.Provider>
  );
};

export const useSocket = () => useContext(SocketContext);

export default SocketProvider;
