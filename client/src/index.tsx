import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import SocketProvider from './contexts/socketContext';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <SocketProvider>
      <App />
    </SocketProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
